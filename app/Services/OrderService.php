<?php

namespace App\Services;

use App\Client;
use App\Order;
use Illuminate\Database\Eloquent\Model;

class OrderService
{
    private $client;
    private $order;

    public function __construct(
        Client $client,
        Order $order
    )
    {
        $this->client = $client;
        $this->order = $order;
    }

    public function create(array $attributes): Model
    {
        $client = $this->client->firstOrCreate(
            ['phone' => $attributes['phone']],
            ['name' => $attributes['name']]
        );

        $order = $this->order->create([
            'delivery_day' => $attributes['delivery_date'],
            'address' => $attributes['address'],
            'tariff_id' => $attributes['tariff_id'],
            'client_id' => $client->id,
        ]);

        return $order;
    }
}