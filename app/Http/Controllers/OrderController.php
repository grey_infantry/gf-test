<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrder;
use App\Services\OrderService;
use App\Tariff;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

class OrderController extends Controller
{
    private $tariff;
    private $orderService;

    public function __construct(Tariff $tariff, OrderService $orderService)
    {
        $this->tariff = $tariff;
        $this->orderService = $orderService;
    }

    public function create(): View
    {
        $tariffs = $this->tariff->all();

        return view('orders.create', ["tariffs"=>$tariffs]);
    }

    public function store(StoreOrder $request): JsonResponse
    {
        $order = $this->orderService->create($request->validated());
        $order->load(['tariff', 'client']);

        $returnHTML = view('orders.store', ['order'=> $order])->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }

}
