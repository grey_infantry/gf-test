<?php

namespace App\Http\Requests;

use App\Tariff;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class StoreOrder extends FormRequest
{
    protected $tariff;

    public function __construct(
        array $query = [], array $request = [], array $attributes = [],
        array $cookies = [], array $files = [], array $server = [], $content = null,
        Tariff $tariff
    )
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->tariff = $tariff;
    }

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'phone' => 'required|string',
            'address' => 'required|string',
            'tariff_id' => 'required|exists:tariffs,id',
            'delivery_date' => 'required|date_format:Y-m-d|after:yesterday',
        ];
    }

    public function withValidator(Validator $validator): void
    {
        $validator->after(function (Validator $validator) {
            $attributes = $this->request->all();

            if (!$this->isDeliveryDateOk($attributes['tariff_id'], $attributes['delivery_date'])) {
                $validator->errors()->add('delivery_date', 'Invalid date');
            }
        });
    }

    private function isDeliveryDateOk(int $tariffId, string $deliveryDate): bool
    {
        $tariff = $this->tariff->findOrFail($tariffId);
        $deliveryWeekday = Carbon::parse($deliveryDate)->dayOfWeekIso;

        return in_array($deliveryWeekday, $tariff->delivery_weekdays);
    }
}
