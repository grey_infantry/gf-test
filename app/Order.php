<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'address',
        'delivery_date',
        'client_id',
        'tariff_id',
    ];

    protected $dates = [
        'delivery_date',
        'created_at',
        'updated_at',
    ];

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function tariff()
    {
        return $this->belongsTo('App\Tariff');
    }
}
