<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'name',
        'phone',
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function setPhoneAttribute($value)
    {
        $this->attributes['phone'] = preg_replace('/[^0-9]/', '', $value);
    }
}
