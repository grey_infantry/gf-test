<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tariff extends Model
{
    protected $casts = [
        'delivery_weekdays' => 'object',
    ];

    protected $fillable = [
        'name',
        'price',
        'delivery_weekdays',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function orders()
    {
        return $this->belongsToMany('App\Order');
    }
}
