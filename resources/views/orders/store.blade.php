@extends('layout')

@section('content')
    <li>Order ID: {{$order->id}}</li>
    <li>Client ID: {{$order->client->id}}</li>
    <li>Client Name: {{$order->client->name}}</li>
    <li>Client Phone: {{$order->client->phone}}</li>
    <li>Delivery Day: {{$order->delivery_date}}</li>
    <li>Address: {{$order->address}}</li>
    <li>Tariff: {{$order->tariff->name}}</li>
    <li>Price: {{ number_format(($order->tariff->price/100), 2) }} ₽</li>
@endsection