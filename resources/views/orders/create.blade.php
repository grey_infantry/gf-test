@extends('layout')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="card uper">
        <div class="card-header">
            Create an order
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br/>
            @endif
            <form id="orderForm" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    @csrf
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name" placeholder="Enter your name" required/>
                </div>
                <div class="form-group">
                    <label for="phone">Phone:</label>
                    <input type="tel" class="form-control" name="phone" placeholder="79993332211" required/>
                </div>
                <div class="form-group">
                    <label for="address">Address:</label>
                    <input type="text" class="form-control" name="address" placeholder="Enter your address" required/>
                </div>
                <div class="form-group">
                    <label for="tariff">Tariff</label>
                    <select id="tariff_id" name="tariff_id" class="form-control">
                        <option value="">Select tariff</option>
                        @foreach ($tariffs as $tariff)
                            <option value="{{ $tariff->id }}"
                                    data-tariff={{json_encode($tariff->delivery_weekdays)}}>{{ $tariff->name }}
                                – {{ number_format(($tariff->price/100), 2) }}₽
                            </option>
                        @endforeach
                    </select>
                </div>
                <div id="delivery_date" class="form-group" style="display:none">
                    <label for="delivery_date">Delivery date</label>
                    <input id="delivery_date_input" type="text" class="date form-control" name="delivery_date"/>
                </div>
                <div>

                </div>

                <button id="submit" type="submit" class="btn btn-primary" style="display:none">Create</button>
            </form>
        </div>
    </div>

    <script type="text/javascript">
      let days, since;
      // Init datepicker with enabled days from tariff
      $('#tariff_id').change(function () {
        days = $(this).find(':selected').attr('data-tariff')

        $('.date').datepicker('remove');
        $('.date').datepicker({
          format: 'yyyy-mm-dd',
          weekStart: 1,
          beforeShowDay: function (date) {
            if (days && !days.includes(date.getDay())) {
              return {
                enabled: false
              };
            }
          }
        });

        // Flush input data after datepicker reload
        $('#delivery_date_input').val('')

        if (days) {
          $('#delivery_date').show()
        } else {
          $('#delivery_date').hide()
        }
      });

      $('#delivery_date').change(function () {
        since = $('#delivery_date_input').val()

        if (days && since) {
          $('#submit').show()
        } else {
          $('#submit').hide()
        }
      });

      // AJAX request to store order
      $(document).ready(function () {
        $('#orderForm').on('submit', function (e) {
          e.preventDefault();

          $.ajax({
            type: 'POST',
            url: '/',
            data: $('#orderForm').serialize(),
            success: function (data) {
              console.log(data)
              if (data.success) {
                $('.container').html(data.html);
              } else {
              }
            },
            error: function () {
            }
          });
        });
      });
    </script>
@endsection