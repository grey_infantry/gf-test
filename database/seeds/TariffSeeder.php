<?php

use Illuminate\Database\Seeder;

class TariffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        if (DB::table('tariffs')->count() > 0) {
            echo("table not empty\n");
            return;
        }

        $date = \Carbon\Carbon::now();

        $tariffs = [
            [
                'name' => 'basic',
                'price' => 100000,
                'delivery_weekdays' => '[1, 3, 5]',
                'created_at' => $date,
            ],
            [
                'name' => 'standard',
                'price' => 200000,
                'delivery_weekdays' => '[1, 2, 3, 4, 5]',
                'created_at' => $date,
            ],
            [
                'name' => 'extra',
                'price' => 300000,
                'delivery_weekdays' => '[1, 2, 3, 4, 5, 6, 7]',
                'created_at' => $date,
            ],
        ];

        foreach ($tariffs as $tariff) {
            DB::table('tariffs')->insert($tariff);
        }
    }
}
