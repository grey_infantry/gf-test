**How to start app**
```
docker-compose up -d
docker-compose exec app bash
```

```
composer install
php artisan migrate
php artisan db:seed
```

**Order form demo**
![](order-demo-original.gif)